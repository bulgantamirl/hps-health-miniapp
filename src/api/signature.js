import { JSEncrypt } from "jsencrypt";
import qs from "qs";

const key = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvzuL2xaalMS8SAoOSFcv
ys34PmjQbsEV62tiNPglwo1n0JQleHd2RMm7aAZpoBiUOpVtMGLc9IfX4O4nwKyE
3i+wAOeYLdZJUroqz01uc+mecr1btn4C78Cxx5ghJFBbEFaZXPcmA5+YBgP9O467
/YCBwEvtUeuK5u5zK9i8dA0H+ttrTIw1VgKtFtJ4IOUKjT0wV1TGobEFd7+Fx2xP
vFuWWucl4viUM7aoN3tvbcAYPmJHUU2X4LPkhHdpYUIXKhMtyc0a1LAw/MAuPIe5
vUNj7r634XQNhBcZAheQFWo8zFVcWCvbnUEo3sIvUJqVheCGISa29Y3e/kF0opSm
IwIDAQAB
-----END PUBLIC KEY-----`;

export const encrypt = (data) => {
  let RSAEncrypt = new JSEncrypt();
  RSAEncrypt.setPublicKey(key);
  return RSAEncrypt.encrypt(data);
};

export const generateXAPISignature = (requestData, url) => {
  try {
    let signature = `${url}`;
    const conjunction = url?.indexOf("?") > 0 ? "&" : "?";
    signature += conjunction;
    if (typeof requestData === "object") {
      signature += `${qs.stringify(requestData, { skipNulls: true })}&`;
    } else {
      signature = encodeURI(signature);
    }
    if (signature.length > 200) {
      signature = signature.slice(0, 199) + "&";
    }
    signature += `request_time=${Math.floor(Date.now() / 1000)}&`;

    signature = encrypt(signature);

    return signature;
  } catch (e) {
    return "";
  }
};
