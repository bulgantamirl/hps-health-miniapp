import { generateXAPISignature } from "api/signature";
import axios from "axios";
import { config } from "config";

const CLIENT_ID = "miniapps";

export const apiClient = axios.create({
  baseURL: config.apiBaseUrl,
  timeout: 65000,
  headers: {
    client_id: CLIENT_ID,
  },
});

apiClient.interceptors.request.use(
  (config) => {
    config.headers["X-Api-Signature"] = generateXAPISignature(
      config.data,
      config.url
    );

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export const setToken = (token) => {
  if (token) {
    apiClient.defaults.headers.common.Authorization = `Bearer ${token}`;
  }
};

export const getAccessToken = () => {
  return apiClient.defaults.headers.common.Authorization?.split(" ")?.[1];
};
