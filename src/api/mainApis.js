import { apiClient } from "./index";

export const mainApis = {
  async fetchUser() {
    return (await apiClient.get('profile'))?.data
  }
}