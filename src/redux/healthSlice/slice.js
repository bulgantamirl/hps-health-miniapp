import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import { healthApis } from "features/health/api";

const startPending = (state) => {
  if (!state.loading) {
    state.loading = true;
  }
};

const stopPending = (state) => {
  if (state.loading) {
    state.loading = false;
  }
};

export const HEALTH_INIT_SLICE = {
  loading: false,
  username: "",
  usernameChecked: false,
  products: [],
  recommendedUsernames: [],
  fetchingRecommendedUsernames: false,
  currentTab: 0,
  drivers: [],
  formValues: {
    carNumber: "",
    driverCardId: "",
    driverExp: "",
    province: "",
    district: "",
    plateLetter: "",
    plateEnding: "",
    guardianType: "",
    ownerType: "personal",
    guardianPhoneNumber: "",
    user: {},
    company: {},
    isCarOwner: false,
    hasTrailer: false,
    vehicleData: {},
  },
};

const healthSlice = createSlice({
  name: "health",
  initialState: HEALTH_INIT_SLICE,
  reducers: {
    setPaymentInfo: (state, action) => {
      // state.ottPaymentInfo = action.payload;
    },
    resetState: (state, action) => {
      state = {
        loading: false,
        type: null,
        invoice: null,
      };
    },
    setProducts: (state, action) => {
      state.products = action.payload;
    },
    updateFormValues: (state, action) => {
      state.formValues = { ...state.formValues, ...action.payload };
    },
    setDrivers: (state, action) => {
      state.drivers = [...state.drivers, action.payload];
    },
    deleteDrivers: (state, action) => {
      state.drivers = state.drivers?.filter(
        (item) => item?.registerId !== action?.payload
      );
    },
    toNextTab: (state) => {
      if (state.currentTab < 4) {
        state.currentTab = state.currentTab + 1;
      }
    },
    toPrevTab: (state) => {
      if (state.currentTab > 0) {
        state.currentTab = state.currentTab - 1;
      }
    },
  },
  extraReducers: {
    // [checkUsernameOtt.pending]: startPending,
    // [checkUsernameOtt.fulfilled]: (state, action) => {
    //   if (state.loading) {
    //     state.username = action.payload?.username;
    //     state.customerId = action.payload?.customerId;
    //     state.usernameChecked = action.payload?.code === 1;
    //     state.loading = false;
    //   }
    // },
    // [checkUsernameOtt.rejected]: stopPending,
    // [fetchOttCards.pending]: startPending,
    // [fetchOttCards.fulfilled]: (state, action) => {
    //   if (state.loading) {
    //     state.cards = action.payload?.cards;
    //     state.loading = false;
    //   }
    // },
    // [fetchOttCards.rejected]: stopPending,
    // [fetchUsernameRecommendation.pending]: startPending,
    // [fetchUsernameRecommendation.fulfilled]: (state, action) => {
    //   if (state.loading) {
    //     state.username = action.payload?.username;
    //     state.recommendedUsernames = action.payload?.list;
    //     state.usernameChecked = action.payload?.code === 1;
    //     state.loading = false;
    //   }
    // },
    // [fetchUsernameRecommendation.rejected]: stopPending,
  },
});

export const healthActions = healthSlice.actions;

export const {
  setProducts,
  updateFormValues,
  setDrivers,
  deleteDrivers,
  toPrevTab,
  toNextTab,
} = healthSlice.actions;

export default healthSlice.reducer;
const getHealth = createSelector(
  [(state) => state.healthSlice],
  (health) => health
);

const getProducts = (state) => state.healthSlice.products;
const getFormValues = (state) => state.healthSlice.formValues;
const getDrivers = (state) => state.healthSlice.drivers;
const getCurrentTab = (state) => state.healthSlice.currentTab;
export const productsSelector = createSelector(
  [getProducts],
  (products) => products
);


export const healthSelectors = {
  getHealth,
  getProducts,
  getFormValues,
  getCurrentTab,
};
