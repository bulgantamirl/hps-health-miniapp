import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import { mainApis } from "api/mainApis";

const initialState = {
  isAuthorized: false,
  user: {},
  loading: false,
  phoneNumber: "",
};

export const fetchUser = createAsyncThunk("main/user", async () => {
  return await mainApis.fetchUser();
});

export const mainSlice = createSlice({
  name: "main",
  initialState,
  reducers: {
    reducer(state = initialState, action) {
      switch (action.type) {
        case "UPDATE_PHONENUMBER":
          return { ...state, phoneNumber: action?.payload };
        default:
          return state;
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUser.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchUser.fulfilled, (state, action) => {
        state.user = action.payload;
        state.isAuthorized = true;
        state.loading = false;
      })
      .addCase(fetchUser.rejected, (state) => {
        state.loading = false;
      });
  },
});

const getMain = createSelector([(state) => state.main], (main) => main);

export const mainSelectors = {
  getMain,
};
export default mainSlice.reducer;
