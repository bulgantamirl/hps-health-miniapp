import { configureStore } from "@reduxjs/toolkit";

import mainReducer from "#redux/mainSlice/slice";
import hidaatgalReducer from "#redux/healthSlice/slice"

export const store = configureStore({
  reducer: {
    hidaatgalSlice: hidaatgalReducer,
    main: mainReducer,
  },
});
