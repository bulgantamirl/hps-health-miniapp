import MainWrapper from "components/MainWrapper";
import {healthRoutes} from "router/healthRoutes";

export const MainRoutes = [
  {
    path: "/",
    component: MainWrapper,
    routes: [healthRoutes],
  },
];
