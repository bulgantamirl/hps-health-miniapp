import Health from "features/health";
import Breathe from "features/health/screens/Breathe";
import ColorTest from "features/health/screens/ColorTest";
import HealthMenu from "features/health/screens/HealthMenu";
import LungExercise from "features/health/screens/LungExercise";
import Quiz from "features/health/screens/Quiz";
import WaterInput from "features/health/screens/WaterInput";
import WaterIntake from "features/health/screens/WaterIntake";

export const healthRoutes = {
  path: "/health",
  component: Health,
  routes: [
    {
      path: "/health",
      component: HealthMenu,
      exact: true,
    },
    {
      path: "/health/breathe",
      component: Breathe,
      exact: true,
    },
    {
      path: "/health/water",
      component: WaterIntake,
      exact: true,
    },
    {
      path: "/health/water/customIntake",
      component: WaterInput,
      exact: true,
    },
    {
      path: "/health/colorTest",
      component: ColorTest,
      exact: true,
    },
    {
      path: "/health/quiz",
      component: Quiz,
      exact: true,
    },
    {
      path: "/health/lung",
      component: LungExercise,
      exact: true,
    },
  ],
};
