import  water0 from "assets/water/water0.svg"
import  water1 from "assets/water/water1.svg"
import { colors } from "constants/colors";

export const waterFillingTypes = {
    fill0: 0,
    fill100: 1,
    fill200: 2,
    fill300: 3,
    fill400: 4,
    fill500: 5,
    fill600: 6,
    fill700: 7,
    fill800: 8,
    fill900: 9,
    fill1000: 10,

};

export const waterFillings = {
    [waterFillingTypes.fill0]: {
        svg: water0,
        text: "0мл",
    },
    [waterFillingTypes.fill100]: {
        svg: water1,
        text: "300мл",
    },

};
