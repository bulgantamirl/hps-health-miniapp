import Epass from "assets/operator/epass.svg";
import GmobileSvg from "assets/operator/gmobile.svg";
import LimeSvg from "assets/operator/lime.svg";
import MobicomSvg from "assets/operator/mobicom.svg";
import MongolSatSvg from "assets/operator/mongolsat.svg";
import SkytelSvg from "assets/operator/skytel.svg";
import UnitelSvg from "assets/operator/unitel.svg";
import { colors } from "constants/colors";

export const mobileOperatorTypes = {
  SKYTEL: "skytel",
  GMOBILE: "gmobile",
  MOBICOM: "mobicom",
  UNITEL: "unitel",
  LIME: "lime",
  MONGOLSAT: "mongolsat",
};
export const travelTypes = {
  EPASS: "epass",
};

export const travelInfo = {
  [travelTypes.EPASS]: {
    Svg: Epass,
    mnName: "EPASS",
    logo: require("assets/mobileOperator/skytel.png"),
    calculateDates: [1, 2, 3, 4, 5],
    text: "Сар бүрийн 5-ны өдөр хүртэл төлбөр бодогддог тул төлбөрийн мэдээлэл харах боломжгүй",
    unitColor: "#0006b8",
    dataColor: "#ff4400",
  },
};

export const mobileOperatorInfo = {
  [mobileOperatorTypes.SKYTEL]: {
    Svg: SkytelSvg,
    mnName: "Скайтел",
    logo: require("assets/mobileOperator/skytel.png"),
    calculateDates: [1, 2, 3, 4, 5],
    text: "Сар бүрийн 5-ны өдөр хүртэл төлбөр бодогддог тул төлбөрийн мэдээлэл харах боломжгүй",
    unitColor: "#0006b8",
    dataColor: "#ff4400",
  },
  [mobileOperatorTypes.GMOBILE]: {
    Svg: GmobileSvg,
    mnName: "ЖиМобайл",
    logo: require("assets/mobileOperator/gmobile.png"),
    calculateDates: [1, 2, 3, 4, 5, 6, 7, 8],
    text: "Сар бүрийн 8-ны өдөр хүртэл төлбөр бодогддог тул төлбөрийн мэдээлэл харах боломжгүй",
    unitColor: "#ffb32f",
    dataColor: colors.info,
  },
  [mobileOperatorTypes.MOBICOM]: {
    Svg: MobicomSvg,
    mnName: "Мобиком",
    logo: require("assets/mobileOperator/mobicom.png"),
    calculateDates: [1, 2, 3, 4, 5],
    text: "Сар бүрийн 5-н хүртэл төлбөр бодогддог тул төлбөрийн мэдээлэл харах боломжгүй",
    unitColor: colors.success,
    dataColor: colors.info,
  },
  [mobileOperatorTypes.UNITEL]: {
    Svg: UnitelSvg,
    mnName: "Юнител",
    logo: require("assets/mobileOperator/unitel.png"),
    calculateDates: [1, 2, 3, 4, 5],
    text: "Сар бүрийн 5-н хүртэл төлбөр бодогддог тул төлбөрийн мэдээлэл харах боломжгүй",
    unitColor: colors.success,
    dataColor: colors.info,
  },
  [mobileOperatorTypes.LIME]: {
    Svg: LimeSvg,
    mnName: "LIME",
    logo: require("assets/mobileOperator/lime.png"),
    calculateDates: [],
    text: "",
    unitColor: "#D0FF14",
    dataColor: "#D0FF14",
    textColor: "#000000",
  },
  [mobileOperatorTypes.MONGOLSAT]: {
    Svg: MongolSatSvg,
    mnName: "MONGOLSAT",
    logo: require("assets/mobileOperator/mongolsat.png"),
    calculateDates: [],
    text: "",
    unitColor: "#D0FF14",
    dataColor: "#D0FF14",
    textColor: "#000000",
  },
};
const phone = {
  required: "Утасны дугаараа зөв оруулна уу!",
  minLength: { value: 8, message: "Утасны дугаараа зөв оруулна уу!" },
  maxLength: { value: 8, message: "Утасны дугаараа зөв оруулна уу!" },
  pattern: { value: /^[0-9]*$/, message: "Утасны дугаараа зөв оруулна уу!" },
};
export const rules = {
  phone,
  "skytel-phone": {
    ...phone,
    pattern: {
      value: /^(91|96|90|50|920)[0-9]*$/,
      message: "Та Скайтелын дугаар оруулна уу!",
    },
  },
  "gmobile-phone": {
    ...phone,
    pattern: {
      value: /^(98|93|97|78|83|921)[0-9]*$/,
      message: "Та ЖиМобайлын дугаар оруулна уу!",
    },
  },
  "unitel-phone": {
    ...phone,
    pattern: {
      value: /^(88|89|86|80)[0-9]*$/,
      message: "Та Юнителийн дугаар оруулна уу!",
    },
  },
  "mobicom-phone": {
    ...phone,
    pattern: {
      value: /^(99|95|94|85|51)[0-9]*$/,
      message: "Та Мобикомын дугаар оруулна уу!",
    },
  },
  "lime-phone": {
    ...phone,
    pattern: { value: /^(72)[0-9]*$/, message: "Та Лаймын дугаар оруулна уу!" },
  },
};
