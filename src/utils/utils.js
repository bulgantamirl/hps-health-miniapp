import { format } from "date-fns";

export function formatNumber(
  amount,
  decimalCount = 0,
  decimal = ".",
  thousands = ","
) {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(
      (amount =
        decimalCount > 0
          ? Math.abs(Number(amount) || 0).toFixed(decimalCount)
          : Math.trunc(Number(amount) || 0)),
      10
    ).toString();
    let j = i.length > 3 ? i.length % 3 : 0;

    return (
      negativeSign +
      (j ? i.substr(0, j) + thousands : "") +
      i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
      (decimalCount
        ? decimal +
          Math.abs(amount - i)
            .toFixed(decimalCount)
            .slice(2)
        : "")
    );
  } catch (e) {
    console.log(e);
  }
}
export const getInvestDateType = (type) => {
  switch (type) {
    case "YEAR":
      return "жил";
    case "MONTH":
      return "сар";
    case "DAY":
      return "өдөр";
  }
};
export const plateEndingLetters = [
  "А",
  "Б",
  "В",
  "Г",
  "Д",
  "Е",
  "Ё",
  "Ж",
  "З",
  "И",
  "Й",
  "К",
  "Л",
  "М",
  "Н",
  "О",
  "Ө",
  "П",
  "Р",
  "С",
  "Т",
  "У",
  "Ү",
  "Ф",
  "Х",
  "Ц",
  "Ч",
  "Ш",
  "Щ",
  "Ъ",
  "Ы",
  "Ь",
  "Э",
  "Ю",
  "Я",
];

export const skyTicketUrl = "https://skyticket.mn/";

export const getBirthday = (regno = "") => {
  let year;
  let month;
  let day;
  if (regno.substring(4, 5) === "2" || regno.substring(4, 5) === "3") {
    year = Number("20" + regno.substring(2, 4));
    month = Number(+regno.substring(4, 5) - 2 + regno.substring(5, 6));
    day = Number(regno.substring(6, 8));
  } else {
    year = Number("19" + regno.substring(2, 4));
    month = Number(regno.substring(4, 5) + regno.substring(5, 6));
    day = Number(regno.substring(6, 8));
  }
  return format(new Date(year, month - 1, day), "yyyy-MM-dd");
};
