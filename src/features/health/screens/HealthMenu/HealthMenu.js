
import Icon from "components/Icon";
import { colors } from "constants/colors";
import menuWater from "assets/layer/menuWater.svg";
import menuBreathe from "assets/layer/menuBreathe.svg";
import menuLungs from "assets/layer/menuLungs.svg";
import menuMemorize from "assets/layer/menuMemorize.svg";
import menuQuiz from "assets/layer/menuQuiz.svg";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {Link, useHistory} from "react-router-dom";
import Header from "components/Header";
import {ButtonBase} from "@mui/material";


const HealthMenu = () => {
    const dispatch = useDispatch();
    return (
        <div className={'container mx-auto bg-white flex flex-col items-center  h-full min-h-screen text-[32px] box-border'}>
            <Header  title={'Эрүүл мэнд'} />
            <p className={'font-semibold text-center text-healthText text-lg'}>
                Өөрийн эрүүл мэндээ шалгаарай.
            </p>
            <div className={'flex flex-row gap-3 max-w-[350px] h-[600px] px-6 mt-4 w-full'}>
                <div className={'flex flex-col gap-3 h-full flex-grow'}>
                    <Link to={'health/water'} className={'bg-healthWater py-3 px-3 justify-between flex flex-col rounded-xl flex-grow'} >
                        <img src={menuWater} alt={''} />
                        <h6 className={'text-[16px] font-bold text-black0 my-0'}>
                            Ус бүртгэх
                        </h6>
                    </Link>
                    <Link to={'health/colorTest'} className={'bg-healthColorTest py-3 px-3 justify-between  flex flex-col rounded-xl flex-grow'}>
                        <img src={menuMemorize} alt={''} />
                        <h6 className={'text-[16px] font-bold text-black0 my-0'}>
                            Ой тогтоолт
                        </h6>
                    </Link>
                    <Link to={'health/quiz'} className={'bg-healthQuiz py-3 px-3 justify-between flex flex-col rounded-xl flex-grow'}>
                        <img  src={menuQuiz} alt={''} />
                        <h6 className={'text-[16px] font-bold text-black0 my-0'}>
                            Сонжоо
                        </h6>
                    </Link>
                </div>
                <div className={'flex flex-col gap-3 h-full flex-grow'}>
                    <Link to={"health/lung"} className={'bg-healthLungs py-3 px-3 justify-between  flex flex-col rounded-xl flex-grow'}>
                        <img  src={menuLungs} alt={''} />
                        <h6 className={'text-[16px] font-bold text-black0 my-0'}>
                            Уушгины багтаамж
                        </h6>
                    </Link>
                    <Link to={"health/breathe"} className={'bg-healthBreathe py-3 px-3 justify-between flex flex-col rounded-xl flex-grow'}>
                        <img  src={menuBreathe} alt={''} />
                        <h6 className={'text-[16px] font-bold text-black0 my-0'}>
                            Амьсгалын дасгал
                        </h6>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default HealthMenu;
