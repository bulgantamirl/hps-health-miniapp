import { ButtonBase } from "@mui/material";
import waterLayer1 from "assets/layer/waterLayer1.svg";
import waterLayer2 from "assets/layer/waterLayer2.svg";
import water1 from "assets/water/water1.svg";
import waterBg from "assets/water/waterBg.svg";
import Header from "components/Header";
import WaterFilling from "features/health/components/WaterFilling";
import WaterFillingModal from "features/health/components/WaterFillingModal";
import WaterGlass from "features/health/components/WaterGlass";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const WaterIntake = () => {
  const history = useHistory();
  const dailyWaterLimit = 3000;
  const dataKey = "dailyWaterIntake";
  const [waterIntake, setWaterIntake] = useState(0);
  const [open, setOpen] = useState(false);
  const [addingWater, setAddingWater] = useState(0);
  function isDataExpired(storedData) {
    if (storedData) {
      const storedDate = new Date(storedData.date);
      const currentDate = new Date();

      // Compare the dates (ignoring time) to check if it's the next day
      if (
        storedDate.getDate() !== currentDate.getDate() ||
        storedDate.getMonth() !== currentDate.getMonth() ||
        storedDate.getFullYear() !== currentDate.getFullYear()
      ) {
        return true; // Data is expired
      }
    }
    return false; // Data is not expired or doesn't exist
  }

  const addWaterIntake = (water) => {
    if (waterIntake >= dailyWaterLimit) {
      console.log("udriin uuh hemje duurev");
    } else {
      setWaterIntake((current) => current + water);
      setAddingWater(water);
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
      }, 2000);
    }
  };

  function saveData() {
    const currentDate = new Date();
    const dataToStore = { date: currentDate.toString(), waterIntake };
    localStorage.setItem(dataKey, JSON.stringify(dataToStore));
  }

  useEffect(() => {
    const storedData = localStorage.getItem(dataKey);
    if (isDataExpired(storedData)) {
      localStorage.removeItem(dataKey);
      saveData();
    }
  });

  const navigateToInput = () => {
    history.push({
      pathname: "water/customIntake",
      state: {},
    });
  };
  return (
    <div
      className={
        "container relative mx-auto bg-healthBreathe flex flex-col items-center  h-full min-h-screen text-[32px] box-border z-0"
      }
    >
      <Header title={"Ус"} />
      <WaterFillingModal open={open} setOpen={setOpen} water={addingWater} />
      <img
        className={"absolute w-full top-0 right-0 left-0 z-1"}
        src={waterLayer1}
        alt={""}
      />
      <img
        className={"absolute w-full bottom-[-40px] right-0 left-0 z-1"}
        src={waterLayer2}
        alt={""}
      />
      <div
        className={
          "w-full h-full flex flex-col items-center px-6 pb-6 z-10 gap-3 flex-grow"
        }
      >
        <h3
          className={
            "font-semibold text-start text-healthText mb-1 w-full text-[22px]"
          }
        >
          Усаа уусан уу?
        </h3>
        <p
          className={
            "w-full font-medium text-start text-[14px] text-healthText"
          }
        >
          Та өнөөдөр уусан усны хэмжээгээ бүртгэж оруулна уу.
        </p>
        <WaterFilling
          waterIntake={waterIntake}
          dailyWaterLimit={dailyWaterLimit}
        />
        <div
          className={
            "w-full flex flex-col justify-between items-center flex-grow"
          }
        >
          <WaterGlass
            waterIntake={waterIntake}
            dailyWaterLimit={dailyWaterLimit}
          />
          <div className={"flex flex-col gap-4 py-3 w-full items-center"}>
            <div
              className={
                "w-full flex flex-row gap-3 justify-center items-center"
              }
            >
              <ButtonBase
                onClick={() => addWaterIntake(100)}
                className={
                  "p-3 bg-white font-medium rounded-xl border-blue-600 border-solid border-[1px]"
                }
              >
                100мл
              </ButtonBase>
              <ButtonBase
                onClick={() => addWaterIntake(200)}
                className={
                  "p-3 bg-white font-medium rounded-xl border-blue-600 border-solid border-[1px]"
                }
              >
                200мл
              </ButtonBase>
              <ButtonBase
                onClick={() => addWaterIntake(500)}
                className={
                  "p-3 bg-white font-medium rounded-xl border-blue-600 border-solid border-[1px]"
                }
              >
                500мл
              </ButtonBase>
              <ButtonBase
                onClick={() => addWaterIntake(1000)}
                className={
                  "p-3 bg-white font-medium rounded-xl border-blue-600 border-solid border-[1px]"
                }
              >
                1000мл
              </ButtonBase>
            </div>
            <ButtonBase
              className={
                "w-full bg-waterBlue rounded-xl py-3 font-medium text-white"
              }
              onClick={() => navigateToInput()}
            >
              БИЧИЖ ОРУУЛАХ
            </ButtonBase>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WaterIntake;
