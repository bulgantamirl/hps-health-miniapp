import { ButtonBase } from "@mui/material";
import waterLayer1 from "assets/layer/waterLayer1.svg";
import waterLayer2 from "assets/layer/waterLayer2.svg";
import water1 from "assets/water/water1.svg";
import waterBg from "assets/water/waterBg.svg";
import Header from "components/Header";
import WaterFilling from "features/health/components/WaterFilling";
import WaterFillingModal from "features/health/components/WaterFillingModal";
import WaterGlass from "features/health/components/WaterGlass";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const WaterInput = () => {
  const dispatch = useDispatch();
  const dailyWaterConsume = 3000;
  const [waterIntake, setWaterIntake] = useState(0);
  const [open, setOpen] = useState(false);
  const [addingWater, setAddingWater] = useState(0);
  const addWaterIntake = (water) => {
    if (waterIntake >= dailyWaterConsume) {
      console.log("udriin uuh hemje duurev");
    } else {
      setWaterIntake((current) => current + water);
      setAddingWater(water);
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
      }, 2000);
    }
  };
  return (
    <div
      className={
        "container relative mx-auto bg-healthBreathe flex flex-col items-center h-full min-h-screen text-[32px] box-border z-0"
      }
    >
      <Header title={"Ус"} />
      <img
        className={"absolute w-full top-0 right-0 left-0 z-1"}
        src={waterLayer1}
        alt={""}
      />
      <img
        className={"absolute w-full bottom-[-40px] right-0 left-0 z-1"}
        src={waterLayer2}
        alt={""}
      />
      <div
        className={
          "w-full h-full flex flex-col items-center px-6 pb-6 z-10 gap-3 flex-grow"
        }
      >
        <h3
          className={
            "font-semibold text-start text-healthText mb-1 w-full text-[22px]"
          }
        >
          Усаа уусан уу?
        </h3>
        <p
          className={
            "w-full font-medium text-start text-[14px] text-healthText"
          }
        >
          Та өнөөдөр уусан усны хэмжээгээ бүртгэж оруулна уу.
        </p>

        <div
          className={
            "w-full flex flex-col justify-between items-center flex-grow"
          }
        >
          <input />
          <div className={"flex flex-col gap-4 py-3 w-full items-center"}>
            <ButtonBase
              className={
                "w-full bg-waterBlue rounded-xl py-3 font-medium text-white"
              }
            >
              БИЧИЖ ОРУУЛАХ
            </ButtonBase>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WaterInput;
