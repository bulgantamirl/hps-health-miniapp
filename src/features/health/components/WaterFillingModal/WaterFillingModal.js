import { Modal } from "@mui/material";
import water1 from "assets/water/water1.svg";
import waterBg from "assets/water/waterBg.svg";
import Icon from "components/Icon";
import { colors } from "constants/colors";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const WaterFillingModal = ({ open, setOpen, water, ...props }) => {
  const dispatch = useDispatch();
  return (
    <div className={"w-full "}>
      <Modal open={open} onClose={() => setOpen(false)}>
        <div
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            backgroundColor: "white",
            padding: "20px",
            width: "100%",
            maxWidth: "315px",
            borderRadius: 8,
          }}
        >
          <p className={"text-[18px] font-semibold text-center"}>
            {water}мл ус амжилттай бүртгэгдлээ.
          </p>
        </div>
      </Modal>
    </div>
  );
};

export default WaterFillingModal;
