import water1 from "assets/water/water1.svg";
import waterBg from "assets/water/waterBg.svg";
import Icon from "components/Icon";
import { colors } from "constants/colors";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const WaterGlass = ({ waterIntake, ...props }) => {
  const dispatch = useDispatch();
  return (
    <div className={"w-full flex flex-col items-center"}>
      <div
        className={
          "w-full mt-3 flex flex-col justify-center items-center z-3 relative flex-grow"
        }
      >
        <img className={"w-[80%] max-w-[300px] z-1"} src={waterBg} alt={""} />
        <img
          className={"w-[135px] absolute max-w-[300px] z-1"}
          src={water1}
          alt={""}
        />
      </div>
    </div>
  );
};

export default WaterGlass;
