import Icon from "components/Icon";
import { colors } from "constants/colors";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const WaterFilling = ({ waterIntake, dailyWaterLimit, ...props }) => {
  const dispatch = useDispatch();
  return (
    <div className={"w-full flex flex-row gap-2 h-[64px] mt-3"}>
      <div
        className={
          "flex-grow py-2 px-4 flex flex-col justify-between border-[#EFEEFC] border-solid rounded-xl border-[1px] backdrop-blur-[5px] bg-[#ffffff20]"
        }
      >
        <p className={"text-[12px] text-healthText"}>Уусан усны хэмжээ:</p>
        <h5 className={"text-[16px] my-0 font-semibold "}>{waterIntake}мл</h5>
      </div>
      <div
        className={
          "flex-grow py-2 px-4 flex flex-col justify-between border-[#EFEEFC] border-solid rounded-xl border-[1px] backdrop-blur-[5px] bg-[#ffffff20]"
        }
      >
        <p className={"text-[12px] text-healthText"}>Уух усны хэмжээ:</p>
        <h5 className={"text-[16px] my-0 font-semibold "}>
          {dailyWaterLimit}мл
        </h5>
      </div>
    </div>
  );
};

export default WaterFilling;
