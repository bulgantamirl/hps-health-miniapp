import React from "react";
import { renderRoutes } from "react-router-config";

const Health = ({ route: { routes }, location }) => {
  return <div>{renderRoutes(routes)}</div>;
};

export default Health;
