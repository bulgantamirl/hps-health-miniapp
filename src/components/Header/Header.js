import { ButtonBase, IconButton } from "@mui/material";
import Icon from "components/Icon";
import { colors } from "constants/colors";
import React from "react";
import { useHistory } from "react-router-dom";

const Header = ({ title }) => {
  const history = useHistory();
  const handleBack = () => {
    if (
      location.pathname === "/health"
    ) {
      window.goBack();
    } else {
      history.goBack();
    }
  };
  return (
    <div
      style={{ paddingTop: "max(env(safe-area-inset-top), 24px)" }}
      className=" py-4 w-full px-3 flex flex-row relative z-10 pt-safe  h-[60px] box-border"
    >
      <ButtonBase onClick={handleBack} style={{ margin: 0, padding: 0 }}>
        <Icon
          className={"absolute left-4"}
          icon="ic24-chevron-left"
          size={24}
          color={colors.black100}
        />
      </ButtonBase>
      <div className="text-center  w-full flex items-center justify-center">
        <p className="relative z-10 font-sans text-lg">
          {title || "Hipay"}
        </p>
      </div>
    </div>
  );
};

export default Header;
