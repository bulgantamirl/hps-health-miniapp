import { InputAdornment, TextField } from "@mui/material";
import Icon from "components/Icon";
import RecommAcc from "components/UserInput/components/RecommAcc";
import { colors } from "constants/colors";
import React from "react";

const UserInputOtt = ({
  bgSrc,
  logoSrc,
  mainText,
  secondText,
  recentNumbers,
  recentNumbersLoading,
  phoneNumber,
  setPhoneNumber,
  defaultUsername,
  ...props
}) => {
  return (
    <div className=" flex justify-center   max-w-[350px]">
      <div className="flex flex-col justify-center items-center text-center shadow-2xl rounded-3xl bg-white w-full">
        <div className={"w-full flex justify-center items-center flex-col"}>
          <img alt="" src={bgSrc} className={"w-full"} />
          <img className={"absolute h-14"} alt={""} src={logoSrc} />
        </div>
        <div className="flex flex-col justify-center items-center  mx-6 pb-3">
          <p className="text-2xl text-black70 font-semibold my-4  w-full">
            {mainText}
          </p>
          <TextField
            id="outlined-start-adornment"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value);
            }}
            sx={{ width: "100%", marginBottom: "20px" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Icon icon="ic20-edit" color={colors.black40} size={20} />
                </InputAdornment>
              ),
            }}
          />
          {!recentNumbersLoading && (recentNumbers || []).length !== 0 && (
            <RecommAcc
              value={recentNumbers}
              text={secondText}
              setTextInput={setPhoneNumber}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default UserInputOtt;
