import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import { colors } from "constants/colors";
import React from "react";

const useStyles = makeStyles((theme) => ({
  button: {
    backgroundColor: "#3A81F7",
    color: "white !important",
    borderRadius: 16,
    fontSize: 14,
    padding: "14px 40px",
    display: "flex",
    fontWeight: 500,
    flexDirection: "row",
    gap: "6px",
    justifyContent: "center !important",
    boxShadow: "0px 0px 24px 0px rgba(0, 0, 0, 0.25)",
    justifySelf: "flex-end",
    "&:hover": {
      backgroundColor: "#3A81F7",
    },
  },
  disabled: {
    backgroundColor: colors.gray1,
    color: colors.gray4,
    borderRadius: 4,
    fontSize: 14,
    gap: "6px",
    padding: "16px 40px",
    display: "flex",
    fontWeight: 500,
    flexDirection: "row",
    cursor: "not-allowed !important",
    justifyContent: "center !important",
    boxShadow: "0px 0px 24px 0px rgba(0, 0, 0, 0.25)",
    justifySelf: "flex-end",
    "&:hover": {
      backgroundColor: colors.gray2,
    },
  },
}));

const ButtonInvest = ({ children, loading, ...props }) => {
  const classes = useStyles();
  return (
    <Button
      className={loading ? classes.disabled : classes.button}
      disabled={loading}
      {...props}
    >
      {loading ? (
        <CircularProgress
          size={25}
          sx={{
            color: "white",
            position: "absolute",
            left: "15%",
            marginLeft: "-15px",
          }}
        />
      ) : (
        <></>
      )}
      {children}
    </Button>
  );
};

export default ButtonInvest;
