import { ButtonBase } from "@mui/material";
import React from "react";

const RecentAcc = ({ value, setTextInput, ...props }) => {
  const arrValue = value || [];
  return (
    <div className="">
      <div className="border-[1px] border-solid border-gray-200 mb-3  rounded flex " />
      <div className="text-start   ">
        <p className="font-bold">{props.text} </p>
        <div className="overflow-x-scroll  mx-[-10px]">
          <div className="my-3 w-[250px] flex">
            {arrValue?.map((item, index) => (
              <ButtonBase
                key={index}
                className="w-fit"
                onClick={() => setTextInput(item?.username)}
              >
                <div className="bg-black10 rounded-[10px] py-[12px]  px-[24px] mr-2 ">
                  <p>{item?.username}</p>
                </div>
              </ButtonBase>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default RecentAcc;
