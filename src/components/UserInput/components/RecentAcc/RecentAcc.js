import { ButtonBase } from "@mui/material";
import React from "react";

const RecentAcc = ({ value, setTextInput, type, ...props }) => {
  const arrValue = value || [];

  return (
    <div className="">
      <div className=" mb-3 rounded flex " />
      <div className="text-start  ">
        <div className="overflow-x-scroll flex flex-row gap-2 my-3 w-[250px] ">
          {arrValue?.map((item, index) => (
            <ButtonBase
              key={index}
              className="w-fit"
              onClick={() => setTextInput(type === "iptv" ? item?.account : item?.phone)}
            >
              <div className="bg-black10 bg-white rounded-[10px] py-[12px] px-[24px]">
                <p>{type === 'iptv' ? item.account :  item.phone}</p>
              </div>
            </ButtonBase>
          ))}
        </div>
      </div>
    </div>
  );
};

export default RecentAcc;
