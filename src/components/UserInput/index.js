import { InputAdornment, TextField } from "@mui/material";
import Icon from "components/Icon";
import RecentAcc from "components/UserInput/components/RecentAcc";
import { colors } from "constants/colors";
import React, { useState } from "react";

const UserInput = ({
  src,
  mainText,
  secondText,
  recentNumbers,
  recentNumbersLoading,
  phoneNumber,
  operator,
  setPhoneNumber,
  type,
  placeholder,
  error,
  setError,
  ...props
}) => {
  const handlePhoneNumberValidation = (e) => {
    const valid = e.target.value;
    let isNumberRegex = /^\d+$/;
    if(operator === "mongolsat") {
      if (valid.length > 10) {
        setError("Картын дугаарын хязгаар 10 орон байна.");
      } else if (valid === "") {
        setPhoneNumber("");
      } else if (isNumberRegex.test(valid)) {
        setPhoneNumber(valid);
        setError("");
      } else {
        setError("Зөвхөн картын дугаар оруулна уу.");
        setPhoneNumber(phoneNumber.replace(/\D/g, ""));
      }
    }
    else {
      if (valid.length > 8) {
        setError("Утасны дугаарын хязгаар 8 орон байна.");
      } else if (valid === "") {
        setPhoneNumber("");
      } else if (isNumberRegex.test(valid)) {
        setPhoneNumber(valid);
        setError("");
      } else {
        setError("Зөвхөн утасны дугаар оруулна уу.");
        setPhoneNumber(phoneNumber.replace(/\D/g, ""));
      }
    }

  };

  return (
    <div className=" flex flex-col justify-center w-full max-w-[400px]">
      <div
        style={{ transition: "0.3s" }}
        className="flex flex-col justify-center overflow-hidden items-center text-center shadow-lg rounded-3xl bg-white w-full"
      >
        <div className={"w-full flex justify-center items-center flex-col"}>
          <img alt="" src={src} className={"w-full"} />
        </div>
        <div className="flex flex-col justify-center items-center w-full px-6 pb-3">
          <p className="text-[22px] text-black70 font-semibold my-4  w-full">
            {mainText}
          </p>
          <TextField
            id="outlined-start-adornment"
            value={phoneNumber}
            onChange={(e) => handlePhoneNumberValidation(e)}
            sx={{ width: "100%", marginBottom: "12px" }}
            required
            placeholder={placeholder}
            error={error !== ""}
            InputProps={{
              style: {
                borderRadius: 8,
                paddingTop: 0,
                paddingBottom: 0,
                height: 48,
              },
              startAdornment: (
                <InputAdornment position="start">
                  <Icon icon="ic20-edit" color={colors.black40} size={20} />
                </InputAdornment>
              ),
            }}
          />
          <p className={"text-xs text-amber-800 max-w-full min-h-2"}>
            {error || ""}
          </p>

        </div>
      </div>
      {!recentNumbersLoading && (recentNumbers || []).length !== 0 && (
        <RecentAcc
          value={recentNumbers}
          text={secondText}
          setTextInput={setPhoneNumber}
          type={type}
        />
      )}
    </div>
  );
};

export default UserInput;
