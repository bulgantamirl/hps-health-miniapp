import React from "react";
import { renderRoutes } from "react-router-config";

const MainWrapper = ({ route: { routes }, location }) => {
  return <div className="h-screen">{renderRoutes(routes)}</div>;
};

export default MainWrapper;
