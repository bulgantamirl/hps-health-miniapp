import Icon from "components/Icon";
import React from "react";

const AlertTag = ({ text, color, bg, ...props }) => {
  return (
    <div
      className={`bg-[#FFB14026] text-[#FFB140] rounded-[8px] p-2 flex flex-row gap-2`}
      {...props}
    >
      <div
        className={
          "bg-[#FFB140] h-[32px] w-[32px] min-w-[32px] rounded-[8px] flex items-center justify-center"
        }
      >
        <Icon icon="ic20-warning" color={"#fff"} size={24} />
      </div>
      <p className={"text-[12px] "}>{text}</p>
    </div>
  );
};

export default AlertTag;
