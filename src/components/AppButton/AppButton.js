import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import { colors } from "constants/colors";
import React from "react";

const useStyles = makeStyles((theme) => ({
  button: {
    backgroundColor: colors.purplecorr,
    color: "white !important",
    borderRadius: 4,
    fontSize: 14,
    padding: "12px 24px",
    width: "100% !important",
    display: "flex",
    fontWeight: 500,
    flexDirection: "row",
    justifyContent: "center !important",
    justifySelf: "flex-end",
    "&:hover": {
      backgroundColor: colors.purplecorr,
    },
  },
  disabled: {
    backgroundColor: colors.gray1,
    color: colors.gray4,
    borderRadius: 4,
    fontSize: 14,
    padding: "12px 24px",
    width: "100% !important",
    display: "flex",
    fontWeight: 500,
    flexDirection: "row",
    cursor: "not-allowed !important",
    justifyContent: "center !important",
    justifySelf: "flex-end",
    "&:hover": {
      backgroundColor: colors.gray2,
    },
  },
}));

const AppButton = ({ children, loading, ...props }) => {
  const classes = useStyles();
  return (
    <Button
      className={loading ? classes.disabled : classes.button}
      disabled={loading}
      {...props}
    >
      {loading ? (
        <CircularProgress
          size={25}
          sx={{
            color: "white",
            position: "absolute",
            left: "15%",
            marginLeft: "-15px",
          }}
        />
      ) : (
        <></>
      )}
      {children}
    </Button>
  );
};

export default AppButton;
