import "./App.css";

import { setToken } from "api";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { renderRoutes } from "react-router-config";
import { BrowserRouter as Router } from "react-router-dom";
import { MainRoutes } from "router";

import { setProducts } from "#redux/healthSlice/slice";
import { fetchUser, mainSelectors } from "#redux/mainSlice/slice";

function PlatformChecker() {
  const userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    // The user agent string contains "iPad", "iPhone", or "iPod", which indicates an iOS device
    return "ios";
  } else if (/android/i.test(userAgent)) {
    // The user agent string contains "Android", which indicates an Android device
    return "android";
  } else {
    // Neither iOS nor Android was detected
    return "unknown";
  }
}

function App() {
  const { isAuthorized, user } = useSelector(mainSelectors.getMain);
  const dispatch = useDispatch();
  const handleToken = () => {
    if (window?.ReactNativeWebView?.postMessage) {
      window.ReactNativeWebView.postMessage(JSON.stringify({ type: "Token" }));
    }
  };
  const getProducts = () => {
    if (window?.ReactNativeWebView?.postMessage) {
      window.ReactNativeWebView.postMessage(
        JSON.stringify({ type: "Products" })
      );
    }
  };

  (PlatformChecker() === "android" ? document : window).addEventListener(
    "message",
    (event) => {
      if (event?.data && typeof event?.data === "string") {
        let messageData = {};
        try {
          messageData = JSON.parse(event.data);
        } catch (e) {
          messageData = {};
        }
        if (messageData.type === "token") {
          const token = messageData.token;
          if (token?.length > 0) {
            setToken(token);
            dispatch(fetchUser());
          }
        } else if (messageData.type === "products") {
          const products = messageData.products;
          if (products?.length > 0) {
            dispatch(setProducts(products));
          }
        }
      }
    }
  );

  useEffect(() => {
    handleToken();
    getProducts();
  }, []);
  return <Router>{!isAuthorized ? renderRoutes(MainRoutes) : null}</Router>;
}

export default App;
