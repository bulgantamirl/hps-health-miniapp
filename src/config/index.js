const sharedConstants = {};
const testConstants = {
  apiBaseUrl: "https://testapi.hipay.mn/",
  isTest: true,
};

const prodConstants = {
  apiBaseUrl: "https://appapi.hipay.mn/",
  isTest: false,
};
export const config = {
  ...sharedConstants,
  ...testConstants,
  // ...prodConstants,
};
