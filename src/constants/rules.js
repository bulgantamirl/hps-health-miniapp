const phone = {
  required: "Утасны дугаараа зөв оруулна уу!",
  minLength: { value: 8, message: "Утасны дугаараа зөв оруулна уу!" },
  maxLength: { value: 8, message: "Утасны дугаараа зөв оруулна уу!" },
  pattern: { value: /^[0-9]*$/, message: "Утасны дугаараа зөв оруулна уу!" },
};

export const rules = {
  // email: {
  //   required: "Та имэйлээ зөв оруулна уу.",
  //   pattern: {
  //     value:
  //       /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  //     message: "Та имэйлээ зөв оруулна уу.",
  //   },
  // },

  lastname: {
    required: "Овогоо оруулна уу!",
    minLength: { value: 1, message: "Овогоо оруулна уу!" },
    validate: (value) => !!value.trim() || "Овогоо оруулна уу!",
  },
  firstname: {
    required: "Нэрээ оруулна уу!",
    minLength: { value: 2, message: "Нэрээ зөв оруулна уу!" },
    validate: (value) => !!value.trim() || "Нэрээ зөв оруулна уу!",
  },
  passport: {
    required: "Пасспортын дугаар оруулна уу!",
    minLength: { value: 1, message: "Пасспортын дугаар оруулна уу!" },
    validate: (value) => !!value.trim() || "Пасспортын дугаар оруулна уу!",
  },
  companyReg: {
    required: "Регистрийн дугаараа оруулна уу!",
    minLength: { value: 1, message: "Регистрийн дугаараа оруулна уу!" },
  },
  registerCharOne: {
    required: "Регистрийн дугаарын эхний үсгээ оруулна уу!",
    minLength: {
      value: 1,
      message: "Регистрийн дугаарын эхний үсгээ оруулна уу!",
    },
    maxLength: {
      value: 1,
      message: "Регистрийн дугаарын эхний үсгээ оруулна уу!",
    },
  },
  registerCharTwo: {
    required: "Регистрийн дугаарын хоёр дахь үсгээ оруулна уу!",
    minLength: {
      value: 1,
      message: "Регистрийн дугаарын хоёр дахь үсгээ оруулна уу!",
    },
    maxLength: {
      value: 1,
      message: "Регистрийн дугаарын хоёр дахь үсгээ оруулна уу!",
    },
  },
  registerNumberShort: {
    required: "Зөв оруулна уу!",
    minLength: { value: 8, message: "Регистрийн дугаараа зөв оруулна уу!" },
    maxLength: { value: 8, message: "Регистрийн дугаараа зөв оруулна уу!" },
    pattern: {
      value: /^[0-9]*$/,
      message: "Регистрийн дугаараа зөв оруулна уу!",
    },
  },
  registerNumber: {
    required: "Регистрийн дугаараа зөв оруулна уу!",
    minLength: { value: 10, message: "Регистрийн дугаараа зөв оруулна уу!" },
    maxLength: { value: 12, message: "Регистрийн дугаараа зөв оруулна уу!" },
    pattern: {
      value: /(^[А-Яа-яёЁөӨүҮ]{2}(\d){8}$)|(^[0-9]{12}$)/,
      message: "Регистрийн дугаараа зөв оруулна уу!",
    },
  },

  countryCode: {
    required: "Улсын дугаараа оруулна уу!",
  },
  phone,
  mostPhone: {
    required: "Утасны дугаараа зөв оруулна уу!",
    minLength: { value: 6, message: "Утасны дугаараа зөв оруулна уу!" },
    maxLength: { value: 8, message: "Утасны дугаараа зөв оруулна уу!" },
    pattern: { value: /^[0-9]*$/, message: "Утасны дугаараа зөв оруулна уу!." },
  },
};
