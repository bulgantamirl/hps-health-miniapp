/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    fontSize: {
      xxs: "10px",
      xs: "12px",
      sm: "14px",
      base: "16px",
      lg: "18px",
      "2xl": "24px",
    },
    extend: {
      keyframes: {
        logo: {
          from: { transform: "rotate(0deg)" },
          to: { transform: "rotate(359deg)" },
        },
      },
      bgGradientDeg: {
        40: "40deg",
      },
      animation: {
        logo: "logo 6s infinite linear",
      },
      boxShadow: {
        innerBall: "inset -5px -5px 6px #FFFFFF, inset 5px 5px 6px #E5E7EB",
        outerBall: "-4px -4px 6px #fefefe, 4px 4px 6px #E5E7EB",
        lightBlur: "5px 5px 25px 0px #2222221A",
      },
      dropShadow: {
        darker: "5px 5px 25px rgba(34, 38, 119, 0.25)",
        innerDarker: "inset 5px 5px 25px rgba(34, 38, 119, 0.25)",
      },
      colors: {
        healthText: "#03174C",
        healthWater: "#66BFFF",
        healthLungs: "#1B7DEE",
        healthColorTest: "#FFC15A",
        healthBreathe: "#6ACBEE",
        healthQuiz: "#6CB28E",
        waterBlue: "#41B0FF",
        primary: "#0048A6",
        hipay: "#ff3b63",
        black0: "#ffffff",
        black10: "#f5f7fa",
        black25: "#e5e7eb",
        black40: "#9CA5B0",
        black70: "#414C58",
        black55: "#646E7B",
        black85: "#353F4A",
        black100: "#212932",
        waterBg: "#BCE3FF",
        daatgalBlue: "#0063D7",
        ballGradient:
          "radial-gradient(63.67% 61.25% at 36.25% 21.25%, #FFFFFF 0%, #DBE3EF 100%)",
        yellow: "#FFB300",
        orange: "#FF5D35",
        orange50: "#FF5D3580",
        red: "#F7003A",
        trainBlue: "#1171B7",
        trainSkyTicketBlue: "#0fb4e2",

        trainGray: "#646E7B",
        trainLightBlue: "rgba(17, 113, 183, 0.24)",
      },
    },
  },
  plugins: [require("tailwindcss-safe-area")],
  corePlugins: {
    preflight: false,
  },
};
